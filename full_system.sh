#!/usr/bin/env sh

[ "$(whoami)" = "root" ] && echo "Do not run this script as root. Please run as a non-priviledged user." && exit 1

SCRIPTDIR="$(dirname -- $0)"
GITREPODIR="$HOME/.local/repos"

_install_base_dirs() {
  [ ! -d ~/.config ] && mkdir ~/.config
  [ ! -d ~/.local/bin ] && mkdir -p ~/.local/bin
  [ ! -d ~/.local/share ] && mkdir -p ~/.local/share
  [ ! -d ~/.local/lib ] && mkdir -p ~/.local/lib
  [ ! -d "$GITREPODIR" ] && mkdir -p "$GITREPODIR"
}

_clone_git_repos() {
  [ -z "$(command -v ssh)" ] && sudo pacman --noconfirm -S openssh
  [ ! -d ~/.ssh ] && echo "Can not find the .ssh directory, please add keys" && exit 1
  [ ! -d $GITREPODIR/dotfiles ] && git clone git@gitlab.com:jadecell/dotfiles.git $GITREPODIR/dotfiles
  [ ! -d $GITREPODIR/archmatic ] && git clone git@gitlab.com:jadecell/archmatic.git $GITREPODIR/archmatic
  [ ! -d $GITREPODIR/aslstatus ] && git clone git@gitlab.com:jadecell/aslstatus.git $GITREPODIR/aslstatus
  [ ! -d $GITREPODIR/dmenu ] && git clone git@gitlab.com:jadecell/dmenu $GITREPODIR/dmenu
  [ ! -d $GITREPODIR/dwm ] && git clone git@gitlab.com:jadecell/dwm $GITREPODIR/dwm
  [ ! -d $GITREPODIR/dwmblocks ] && git clone git@gitlab.com:jadecell/dwmblocks $GITREPODIR/dwmblocks
  [ ! -d $GITREPODIR/gentoomatic ] && git clone git@gitlab.com:jadecell/gentoomatic $GITREPODIR/gentoomatic
  [ ! -d $GITREPODIR/installscripts ] && git clone git@gitlab.com:jadecell/installscripts $GITREPODIR/installscripts
  [ ! -d $GITREPODIR/slock ] && git clone git@gitlab.com:jadecell/slock $GITREPODIR/slock
  [ ! -d $GITREPODIR/slstatus ] && git clone git@gitlab.com:jadecell/slstatus $GITREPODIR/slstatus
  [ ! -d $GITREPODIR/st ] && git clone git@gitlab.com:jadecell/st $GITREPODIR/st
  [ ! -d $GITREPODIR/wallpapers ] && git clone git@gitlab.com:jadecell/wallpapers $GITREPODIR/wallpapers
}

_change_ownership_of_homedir() {
  USERNAMEOFUSER="$(whoami)"
  sudo chown -R "$USERNAMEOFUSER":"$USERNAMEOFUSER" ~
}

_install_aur_helper() {
  git clone https://aur.archlinux.org/paru-bin.git && cd paru-bin && makepkg --noconfirm -si
  cd ..
  rm -rf paru-bin
  sudo sed -i -e "s/#BottomUp/BottomUp/g ; s/#SudoLoop/SudoLoop/g ; s/#UpgradeMenu/UpgradeMenu/g" /etc/paru.conf
}

_install_hooks_dir() {
  sudo mkdir -p /etc/pacman.d/hooks
}

_write_changes_to_pacman_configs() {
  CPUTHREADS=$(grep -c processor /proc/cpuinfo)
  CPUTHREADSPLUSONE=$((CPUTHREADS + 1))
  sudo sed -i -e 's/#Color/Color/g ; s/#VerbosePkgLists/VerbosePkgLists/g' /etc/pacman.conf
  sudo sed -i -e 's/#HookDir\ \ \ \ \ =\ \/etc\/pacman.d\/hooks\//HookDir\ \ \ \ \ =\ \/etc\/pacman.d\/hooks\//g' /etc/pacman.conf
  sudo sed -i '37i ILoveCandy' /etc/pacman.conf
  sudo sed -i -e "s/#MAKEFLAGS=\"-j2\"/MAKEFLAGS=\"-j$CPUTHREADSPLUSONE\"/g" /etc/makepkg.conf
  sudo sed -i -e "s|#\ Defaults\ secure_path=\"/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\"|Defaults\ secure_path=\"/usr/local/sbin\:/usr/local/bin\:/usr/sbin\:/usr/bin\:/sbin\:/bin\:/home/$USERNAMEOFUSER/.local/bin\"|g" /etc/sudoers
}

_install_packages() {
  paru --noconfirm -Sy archlinux-keyring
  paru --noconfirm -S libxft-bgra
  PKGLIST="$(sed -e '/^#/d ; s/[\t ]//g ; /^$/d' $SCRIPTDIR/pkglist | tr '\n' ' ')"
  paru --noconfirm -S "$PKGLIST"
}

_enable_services() {
  sudo systemctl enable --now cronie
}

_deploy_symlinks() {
  $GITREPODIR/dotfiles/setup home
}

_switch_default_shell() {
  sudo chsh -s /bin/zsh "$USERNAMEOFUSER"
}

_setup_symlinks_for_profiles() {
  rm ~/.bash_profile
  ln -s $GITREPODIR/dotfiles/home/.config/shell/profile ~/.zprofile
  ln -s $GITREPODIR/dotfiles/home/.config/shell/profile ~/.bash_profile
  ln -s $GITREPODIR/dotfiles/home/.config/x11/xprofile ~/.xprofile
}

_install_zplug() {
  curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh
}

_misc_finalization() {
  sudo chsh -s /bin/zsh "root"
  sudo wget -O /root/.zshrc https://git.grml.org/f/grml-etc-core/etc/zsh/zshrc
  clear
  echo "Everything deployed correctly!"
}

# Run everything
_install_base_dirs
_clone_git_repos
_change_ownership_of_homedir
_install_aur_helper
_install_hooks_dir
_write_changes_to_pacman_configs
_install_packages
_enable_services
_deploy_symlinks
_switch_default_shell
_setup_symlinks_for_profiles
_install_zplug
_misc_finalization
