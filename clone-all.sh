#!/usr/bin/env sh
set -eu

mkdir ~/.config
mkdir -p ~/.local/bin
mkdir -p ~/.local/share
mkdir -p ~/.local/lib
mkdir -p ~/.local/repos

[ ! -d ~/.ssh ] && echo "Can not find the .ssh directory, please add keys" && exit 1
[ ! -d ~/.local/repos/dotfiles ] && git clone git@gitlab.com:jadecell/dotfiles.git ~/.local/repos/dotfiles
[ ! -d ~/.local/repos/archmatic ] && git clone git@gitlab.com:jadecell/archmatic.git ~/.local/repos/archmatic
[ ! -d ~/.local/repos/aslstatus ] && git clone git@gitlab.com:jadecell/aslstatus.git ~/.local/repos/aslstatus
[ ! -d ~/.local/repos/dmenu ] && git clone git@gitlab.com:jadecell/dmenu ~/.local/repos/dmenu
[ ! -d ~/.local/repos/dwm ] && git clone git@gitlab.com:jadecell/dwm ~/.local/repos/dwm
[ ! -d ~/.local/repos/dwmblocks ] && git clone git@gitlab.com:jadecell/dwmblocks ~/.local/repos/dwmblocks
[ ! -d ~/.local/repos/gentoomatic ] && git clone git@gitlab.com:jadecell/gentoomatic ~/.local/repos/gentoomatic
[ ! -d ~/.local/repos/installscripts ] && git clone git@gitlab.com:jadecell/installscripts ~/.local/repos/installscripts
[ ! -d ~/.local/repos/slock ] && git clone git@gitlab.com:jadecell/slock ~/.local/repos/slock
[ ! -d ~/.local/repos/slstatus ] && git clone git@gitlab.com:jadecell/slstatus ~/.local/repos/slstatus
[ ! -d ~/.local/repos/st ] && git clone git@gitlab.com:jadecell/st ~/.local/repos/st
[ ! -d ~/.local/repos/wallpapers ] && git clone git@gitlab.com:jadecell/wallpapers ~/.local/repos/wallpapers
